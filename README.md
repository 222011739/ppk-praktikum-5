> Nama : Muhammad Naufal Faishal            
Kelas / Absen : 3SI3 / 27

## Laporan Praktikum 5

1. Register
        >![Register](/image/register.png "register")
1. Login
        >![login](/image/login.png "login")
1. Get Current User Login
        >![me](/image/me.png "me")
1. Access Protected Resources
    1. With Token
        >![withToken](/image/withToken.png "withToken")
    1. Without Token
        >![withoutToken](/image/withoutToken.png "withoutToken")
    1. Wrong Token
        >![wrongToken](/image/wrongToken.png "wrongToken")



